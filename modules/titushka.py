#!/usr/bin/env python
# coding=utf-8
"""
titushka.py - Phenny Titushka Module
Copyright 2014, Akiyama Mio, 0chan.hk
Licensed under the GPLv2.

http://inamidst.com/phenny/
"""

import base64
import re
import web
import HTMLParser

def get_title(url):
   page = base64.b64encode(url)

   query = ('''exec('import urllib, re\\nfrom BeautifulSoup import BeautifulSoup\\n''' +
            '''addr="""''' + page + '''""".decode("""base64""")\\n''' +
            '''url=addr if re.match("https?://.*", addr) else "http://"+addr\\n''' +
            '''content=urllib.urlopen(url).read()\\n''' +
            '''title = BeautifulSoup(content).title\\n''' +
            '''print title.string.encode("utf-8") if title else "No title for you."')''')

   query_uri = 'http://tumbolia.appspot.com/py/'
   result = web.get(query_uri + web.urllib.quote(query, safe=""))

   if result:
      return HTMLParser.HTMLParser().unescape(result.decode("utf-8")).encode("utf-8")
   else:
      return None

def youtube(phenny, input):
   phenny.message_state["YoutubeProcessed"] = True

   content = get_title(input.group(2).encode('utf-8'))
   if content:
      phenny.say(content)
   else:
      phenny.reply('Sorry, no result.')
youtube.rule = r"(.*\s|)(https?://.*youtube.*/watch\?.*v=[A-Za-z0-9_\-]+).*"
youtube.priority = "medium"
youtube.thread = False

def titushka(phenny, input):
   if phenny.message_state.has_key("YoutubeProcessed"):
      return

   content = get_title(input.group(2).encode('utf-8'))
   if content:
      phenny.say(content)
   else:
      phenny.reply('Sorry, no result.')
titushka.commands = ['titushka']
titushka.priority = "low"
titushka.thread = False

if __name__ == '__main__':
   print __doc__.strip()
