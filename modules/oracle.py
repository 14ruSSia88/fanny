#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
oracle.py - Phenny Oracle Module
Copyright 2014, Govna Kusok
Licensed under WTFPL.

http://0chan.hk/
"""

import re, random

answers = {"Да": 10,
           "Нет": 10,
           "Попробуй еще раз": 10,
           "Иди на хуй": 7,
           "Сомневаюсь": 10,
           "Скорее всего да": 10,
           "Без сомнений": 10,
           "Азаза мамку тваю ибал))": 5}

def choice(seq):
   return random.choice(answers.keys())

   # the counted variant is disabled for now
   pointer = random.randint(1, sum(seq.values()))

   for value, probability in seq.iteritems():
      if pointer <= probability:
         return value
      else:
         pointer -= probability

#measurements = dict([(k, 0) for k in answers.keys()])
#
#for i in xrange(10000):
#   measurements[choice(answers)] += 1
#
#for v, m in  measurements.iteritems():
#   print v.decode("utf-8"), m

def predict(phenny, input): 
   if not phenny.have_replied():
      answer = choice(answers)
      if answer:
         phenny.reply(answer)

predict.rule = ('$nick', r'(.*)\?$')
predict.priority = 'low'

if __name__ == '__main__': 
   print __doc__.strip()
